#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Test script for Shortest_Path_Between_Nodes.py
Created on Tue Jan 19 12:22:25 2021

@author: chrisunderwood
"""
import unittest
import numpy as np
from Shortest_Path_Between_Nodes import graph


class testGraph(unittest.TestCase):
    """ Unit testing class for graph class.
    """
    
    def test_check_input_data(self):
        """ Testing the inputting of the data.
        
        Checking node names, and whether the file entered exists.
        """
        print("\n\tTesting the checking of the input data")
        self.setup_for_testing()
        # Test for correct input data
        self.graph.check_input_data()

        # Test for incorrect node name        
        startNode = 'J10 01'
        self.graph.startNode = startNode
        with self.assertRaises(AssertionError):
            self.graph.check_input_data()

        # Test for file that does not exist
        fileName = 'NOT_A_FILE.dat'
        startNode = 'J1001'
        self.graph.fileName = fileName
        self.graph.startNode = startNode
        with self.assertRaises(AssertionError):
            self.graph.check_input_data()
        
                
    def setup_for_testing(self):
        """ Loading the graph for tests
        """
        self.graph = graph()        
        self.graph.startNode = 'J1001'
        self.graph.endNode = 'J1002'
        self.graph.fileName = 'testing_data/exmouth-links.dat'
        self.graph.loadGraph()
        
    def input_nodes_and_load_graph(self, start, end):
        self.graph.startNode = start
        self.graph.endNode = end
        self.graph.loadGraph()
        
        
    def test_StartNodeNotEndNode(self):
        """ Testing error is raised when start and end are the same
        """
        self.graph = graph()
        self.graph.startNode = 'J1001'
        self.graph.endNode = 'J1001'
        self.graph.fileName = 'testing_data/exmouth-links.dat'
        
        with self.assertRaises(AssertionError):
            self.graph.check_input_data()
        


    def test_loadGraph(self):
        """ Testing the loading of data from file.
        
        Complete networks should load fine, and incomplete will raise errors.
        """
        print("\n\tTesting the loading of the graph data from file")
        self.graph = graph()
        filePath_complete = "testing_data/exmouth-links.dat"
        filePath_incomplete = "testing_data/broken_exmouth-links.dat"
        filePath_incorrect = "testing_data/incorrect_exmouth-links.dat"

        print("Loading correct data")
        self.graph.fileName = filePath_complete
        self.input_nodes_and_load_graph('J1001', 'J1002')        
        self.assertEqual(self.graph.inputData.shape[1], 3, "The loaded data is not the correct shape")

        print("Loading incorrect data, some rows only have 2 entries")
        self.graph.fileName = filePath_incomplete
        with self.assertRaises(AssertionError):
            self.graph.loadGraph()

        print("Loading incorrect data, distance data is not a float")
        self.graph.fileName = filePath_incorrect
        with self.assertRaises(AssertionError):
            self.graph.loadGraph()
            
            
    def test_create_edges_dict(self):
        """ Testing that the vertices of the network are correctly loaded.
        
        Making assumption that all vertices are connected to atleast one other.
        """
        print("\n\tTesting the creation of edges")
        print("Making assumption that all vertices have at least one connection")
        self.setup_for_testing()
        self.graph.create_edges_dict()
        
        for key in self.graph.verticesConnections:
            nLinks = len(self.graph.verticesConnections[key])
            # print (key, nLinks)
            self.assertTrue(nLinks > 0, 'Vertice {} does not have any connections'.format(key))
            
        filePath_short = "testing_data/short_exmouth-links.dat"
        self.graph.fileName = filePath_short
        self.graph.loadGraph()
        with self.assertRaises(AssertionError):
            self.graph.create_edges_dict()


    def test_calc_shortest_path_direct_route(self):
        """ Testing the shortest path is found
        """
        print("\n\tTesting the shortest path algrothim")
        self.graph = graph()
        # A small network for testing.
        self.graph.fileName = "testing_data/test-links.dat"
        
        # Test case 1
        print("Case 1: Find direct shortest path")
        self.input_nodes_and_load_graph('A', 'B')        
        self.graph.calc_shortest_path_Dijkstra()
        self.assertEqual(self.graph.shortestPath, [['A', 'B']],
                          'Cannot find direct shortest path')

    def test_calc_shortest_path_directional_route(self):
        """ Testing the shortest path is found in a directional case
        """
        print("\n\tTesting the shortest path algrothim")
        self.graph = graph()
        # A small network for testing.
        self.graph.fileName = "testing_data/test-links.dat"
        # Test case 2
        #   Direction of ab prevents same path back.
        print("Case 2: Find the directional shortest path")
        self.input_nodes_and_load_graph('B', 'A')        
        self.graph.calc_shortest_path_Dijkstra()
        self.assertEqual(self.graph.shortestPath, [['B', 'C', 'D', 'A']],
                          'Cannot find the directional shortest path')
        
    def test_calc_shortest_path_multiple_routes(self):
        """ Testing the shortest path is found when there are multiple paths.
        """
        print("\n\tTesting the shortest path algrothim")
        self.graph = graph()
        # A small network for testing.
        self.graph.fileName = "testing_data/test-links.dat"
        # Test case 3
        #   Multiple shortest paths
        print("Case 3: Multiple shortest paths")
        self.input_nodes_and_load_graph('A', 'C')
        self.graph.calc_shortest_path_Dijkstra()
        potentialPaths = [['A', 'B', 'C'],
                          ['A', 'E', 'C'],
                          ['A', 'D', 'C']]
        for pathOption in potentialPaths:
            self.assertTrue(pathOption in self.graph.shortestPath,
                             'Has not found all the option')
        
        self.assertEqual(len(self.graph.shortestPath), len(potentialPaths),
                         'The number of found paths is not equal to the number known to exist.')
        
    def test_calc_shortest_path_routes_with_loop(self):
        """ Testing the shortest path is found when there are multiple paths.
        """
        print("\n\tTesting the shortest path algrothim with loop halfway in path")
        self.graph = graph()
        # A small network for testing containing a loop with nodes on each end
        self.graph.fileName = "testing_data/test_4-links.dat"
        potentialPaths = [['G', 'F', 'E', 'A', 'B', 'C', 'H'],
                          ['G', 'F', 'E', 'A', 'D', 'C', 'H']]
        
        self.input_nodes_and_load_graph('G', 'H')
        self.graph.calc_shortest_path_Dijkstra()
        print(self.graph.shortestPath)
        for pathOption in potentialPaths:
            self.assertTrue(pathOption in self.graph.shortestPath,
                             'Has not found all the option\n\t{}'.format(pathOption))

        self.assertEqual(len(self.graph.shortestPath), len(potentialPaths),
                         'The number of found paths is not equal to the number known to exist.')
        
    def test_calc_shortest_path_routes_with_unsymmetrical_loop(self):
        """ Testing the shortest path is found when there are multiple paths.
        
        Different number of nodes in the loop
        """
        print("\n\tTesting the shortest path algrothim with unsymmetrical loop halfway in path")
        self.graph = graph()        
        # A small network for testing containing a loop with nodes on each end
        # Path shortest path has different number of nodes on each route.
        self.graph.fileName = "testing_data/test_5-links.dat"
        potentialPaths = [['G', 'F', 'E', 'A', 'B', 'C', 'H'],
                          ['G', 'F', 'E', 'A', 'D1', 'D', 'C', 'H']]
        
        self.input_nodes_and_load_graph('G', 'H')
        self.graph.calc_shortest_path_Dijkstra()
        print(self.graph.shortestPath)
        for pathOption in potentialPaths:
            self.assertTrue(pathOption in self.graph.shortestPath,
                             'Has not found all the option\n\t{}'.format(pathOption))

        self.assertEqual(len(self.graph.shortestPath), len(potentialPaths),
                         'The number of found paths is not equal to the number known to exist.')        
        
    def test_shortestPathLoops(self):
        """ Testing the shortest path does not contain loops
        
        The shortest path should not go back to the same node.
        """
        print("\n\tTesting the shortest path algrothim does not create loops")
        
        self.graph = graph()
        # A small network for testing.
        self.graph.fileName = "testing_data/test-links.dat"
        self.input_nodes_and_load_graph('A', 'C')
        self.graph.calc_shortest_path_Dijkstra()
        
        for path in self.graph.shortestPath:
            nodes, counts = np.unique(path, return_counts=True)
            for c in counts:
                self.assertEqual(c, 1, 'Node has been found multiple times.')
                
                
    def test_calc_shortest_path_Dijkstra_seperateLoops(self):
        """ Testing the shortest path algorthim raises error when path invalid
        
        In the case of two uncoonected loops, the algorithm should raise an
        AssertionError.
        """
        print("\n\tTesting the shortest path algrothim on seperate loops")
        self.graph = graph()
        # A small network for testing.
        self.graph.fileName = "testing_data/test-2loops.dat"
        
        self.input_nodes_and_load_graph('C', 'A')
        self.graph.calc_shortest_path_Dijkstra()
        self.assertEqual(self.graph.shortestPath, [['C', 'D', 'A']],
                          'Cannot find direct shortest path')        

        self.input_nodes_and_load_graph('F', 'E')
        self.graph.calc_shortest_path_Dijkstra()
        self.assertEqual(self.graph.shortestPath, [['F', 'E']],
                          'Cannot find direct shortest path')                
        
        # Two nodes that are not connected
        self.input_nodes_and_load_graph('A', 'G')
        with self.assertRaises(AssertionError):
            self.graph.calc_shortest_path_Dijkstra()

if __name__ == '__main__':    
    print("Test Script for: Shortest_Path_Between_Nodes.py")    
    unittest.main()
    