#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Shortest route between nodes.

Scientific Programmer Task for City Science.

Created on Tue Jan 19 12:16:00 2021
@author: chrisunderwood

"""
import os
import argparse
import numpy as np
import networkx as nx
import matplotlib as mpl
import matplotlib.pyplot as plt

def setup_plotting(labelSize = 15):
    """ Setup the matplotlib parameters to create more readable plots
    
    Args:
        labelSize: float to define size of labels on plots.
    """
    mpl.rcParams['figure.figsize'] = [10.0, 8.0]
    mpl.rcParams['ytick.labelsize'] = labelSize
    mpl.rcParams['xtick.labelsize'] = labelSize
    mpl.rcParams['axes.labelsize'] = labelSize
    mpl.rcParams['legend.fontsize'] = labelSize
    mpl.rcParams['axes.titlesize'] = labelSize + 2
    mpl.rcParams['legend.title_fontsize'] = labelSize
    mpl.rcParams['figure.titlesize'] = labelSize + 3


class graph():
    """Graph class for working out the shortest route between vertices in a graph.

    The inputs will be the filename and the two nodes to find the shortest path between.
    """
    
    def parser(self):
        """Using argparse gets the file name and start/end node.
        
        The inputs are as follows:
            --fileName/-f       file name of the network
            --startNode/-s      name of the start node
            --endNode/-e        name of the end node
        """   
        parser = argparse.ArgumentParser()
        parser.add_argument("--fileName", "-f", type=str, required=True)
        parser.add_argument("--startNode", "-s", type=str, required=True)
        parser.add_argument("--endNode", "-e", type=str, required=True)
        self.fileName = parser.parse_args().fileName
        self.startNode = parser.parse_args().startNode
        self.endNode = parser.parse_args().endNode
        # Run check on input
        self.check_input_data()
    
    def check_input_data(self):
        """Checks the inputs into the class.
        
        Checks where the filepath entered exists and whether the two nodes are
        in the format expected, a string containing no spaces.
    
        Args:
            None
    
        Returns:
            None
    
        Raises:
            AssertionError: The nodes have been inputted incorrectly.
        """     
        def check_nodes(node, name):
            """ Checks the nodes have len > 0 and no spaces """
            t = node.split(" ")
            errorString = "\n\tNode>{}: {}".format(name, node)
            if len(t) > 1:
                assert False, "Node inputted incorrectly." + errorString
            assert len(node) > 0, "Node has no length." + errorString
                
        errorMessage = "File path provided does not exists\n\tIncorrect input file path: {}".format(self.fileName)
        assert os.path.exists(self.fileName), errorMessage
        check_nodes(self.startNode, "startNode")
        check_nodes(self.endNode, "endNode")
        assert self.startNode != self.endNode, 'The start and the end node are the same.'

    def loadGraph(self):
        """ Loading the graph from self.filename and checking the format is correct.
        
        Loads the data and create a list of all the vertices in the graph.
        
        Raises:
            AssertionError: The graph data file is not in the expected format
                or the start/end nodes are not in the graph.
        """
        self.check_input_data()        
        with open(self.fileName, 'r') as dataFile:
            lines = dataFile.readlines()

        def _check_segments(segments, lineNo):
            # Internal function to check each row to check that it contains 3
            # peices of data. The first two should be node names, and the
            # final the distance between them
            errorString = "\n\tPlease check file {}\n\tLineNumber {}".format(self.fileName, lineNo)
            assert len(segments) == 3, "Incomplete graph data entered." + errorString
            
            for seg in segments:
                # Check each segment is not len = 0
                assert len(seg) > 0, 'There is no data in one of the entries' + errorString
                            
            assert type(float(segments[2])) == float, "The distance is not a number"+ errorString
            
            segments[2] = float(segments[2])
            # Check that the segment corresponding to distance is greater than 0.
            assert float(segments[2]) >= 0., "The distance provided was incorrect." + errorString
            return segments
        
        # List to store the data in
        self.inputData = []
        for lineNo, l in enumerate(lines):
            # Remove new line character
            l = l.strip("\n")
            segments = l.split(" ")
                    
            segments = _check_segments(segments, lineNo)
            self.inputData.append(segments)
            
        # Convert data to np array
        self.inputData = np.array(self.inputData, dtype=object)
        self.vertices = np.unique(self.inputData[:, :2].flatten())
        
        # Check to whether nodes are in graph
        assert self.startNode in self.vertices, "Start node not in Vertices: {}".format(
                self.startNode)
        assert self.endNode in self.vertices, "End node not in Vertices: {}".format(
                self.endNode)
        
        
    def displayInputtedNetwork(self, figsize=(10, 8)):
        """Displays the graph using networkx module.
        
        This displays using matplotlib, and saves the plot to file 
        file name> Network_diagram.png
        
        Args:
            figsize: tuple for the dimensions of the figure
            
        Returns:
            G: the graph produced by networkx
        """             
        plt.figure(figsize=figsize)
        G = nx.DiGraph()
        for s, e, w in self.inputData:
            G.add_edge(s, e, weighth=w)

        # Drawing options:
        #   draw
        #   draw_random
        #   draw_circular
        #   draw_spectral
        nx.draw(G, with_labels=True)

        plt.savefig('Network_diagram.png')
        plt.show()
        return G

    def create_edges_dict(self, printConnections=False):
        """ Loading the graph from self.filename and checking the format is correct.
        
        Loads the data and create a list of all the vertices in the graph.
        Args:
            printConnections: bool, prints the connections per vertex to screen
        """            
        self.verticesConnections = {}
        for v in self.vertices:
            self.verticesConnections[v] = []
            
        for row in self.inputData:
            s, e, d = row
            self.verticesConnections[s].append([e, d])
            
        if printConnections:
            for key in self.verticesConnections:
                print(key)
                for r in self.verticesConnections[key]:
                    print("\t{}\t{}".format(r[0], r[1]))
        
        for key in self.verticesConnections:
            nLinks = len(self.verticesConnections[key])
            # print (key, nLinks)
            assert nLinks > 0, 'Vertice {} does not have any connections'.format(key)
            
        startConnections = self.verticesConnections[self.startNode]
        endConnections = self.verticesConnections[self.endNode]
        
        if printConnections:
            print("Start node: {} connected to {} nodes".format(self.startNode,
                  len(startConnections)))
            print("End node  : {} connected to {} nodes".format(self.endNode,
                  len(endConnections)))
                
    def create_unvisted_and_tentative_distance(self):
        """ Creates lise of unvisited vertices, and the tentative distance
        
        The tentative distance is initialised at infinity for unvisited nodes.
        Returns:
            unvistedNodes: a list of the nodes in the network, bar the start 
                node
                
            tentativeDistance: the tentative distance from the start node to
                that node. Initially infinity.
        """
        unvistedNodes = [x for x in self.vertices if x is not self.startNode]
        tentativeDistance = {}
        for node in unvistedNodes:
            # Store distance and route
            tentativeDistance[node] = [np.inf, []]
        tentativeDistance[self.startNode] = [0, [[self.startNode]]] # Duplicating allowed in list
        return unvistedNodes, tentativeDistance
        
    def calc_shortest_path_Dijkstra(self, debugPrint=False):
        """ Calculates the shortest path using Dijkstra's algorithm
        
        Args:
            debugPrint: bool, displays more information for debugging
    
        Returns:
            shortestPath: a list of the routes of the shortest paths. For most 
                cases there will only be one route, but if multiple routes
                
            shortestPathDistance: the length of the shortest path from the start
                node to the end node.
    
        Raises:
            AssertionError: The nodes have been inputted incorrectly.        
        """
        if debugPrint:
            print("\nCalculating the shortest path in network from:\n\t{}".format(self.fileName))
            print("Between node {} to {}".format(self.startNode, self.endNode))
        

        self.create_edges_dict()            
        unvistedNodes, tentativeDistance = self.create_unvisted_and_tentative_distance()
        
        if debugPrint:
            print("Start Node:", self.startNode)
            print("Unvisited Nodes:", unvistedNodes)
            print("Connections from start")
            for row in self.verticesConnections[self.startNode]:
                print('\t', row)
                
            print("Starting tentativeDistance")
            for key in tentativeDistance:
                print('\t', key, tentativeDistance[key])

        # Start the search from the start node.
        node = self.startNode
        carryOn = True
        while carryOn:
            currentDistance = tentativeDistance[node][0]
            possibleConnections = np.array(self.verticesConnections[node])[:, 0]
            currentpath = tentativeDistance[node][1]

            if debugPrint:
                print("\n  New Loop -> Starting from node: ", node)
                print("Can connect to:", possibleConnections)
            
            for connection in self.verticesConnections[node]:
                nextNode = connection[0]
                nodeDistance = connection[1]

                # Work out distance to node from this route.
                newDistance = currentDistance + nodeDistance
                nodeCanBeVisited = nextNode in unvistedNodes                
                
                if len(currentpath) == 1:
                    # only one route to this node
                    path = np.concatenate((currentpath[0], [nextNode]))
                    path = path.tolist()
                    if debugPrint:
                        print("Current, next", currentpath, nextNode)
                        print("New Path", path)
                    multiplePaths = False
                else:
                    path = []
                    for p in currentpath:
                        pathOption = np.concatenate((p, [nextNode]))
                        pathOption = pathOption.tolist()
                        path.append(pathOption)
                    if debugPrint:
                        print("Multiple paths to this node")
                        print("Current Path, next node", currentpath, nextNode)
                        print("New Path", path)
                    multiplePaths = True

                # Testing the new route.                
                distanceIsLess = newDistance < tentativeDistance[nextNode][0]
                distanceTheSame = newDistance == tentativeDistance[nextNode][0]
                numberOfPaths = len(tentativeDistance[nextNode][1])
                pathAlreadyNoted = path not in tentativeDistance[nextNode][1] and numberOfPaths > 0
                
                if distanceTheSame and pathAlreadyNoted and nodeCanBeVisited:
                    # Duplicate path, need to store both route options.
                    tentativeDistance[nextNode][1].append(path)
                    if debugPrint:
                        print("Duplicate route for", path, tentativeDistance[nextNode][1])                    

                if distanceIsLess and nodeCanBeVisited:
                    # The newest path is the shortest, update tentativeDistance
                    tentativeDistance[nextNode][0] = newDistance

                    # If multiple paths exists do not nest the lists further.
                    if multiplePaths:
                        tentativeDistance[nextNode][1] = path
                    else:
                        tentativeDistance[nextNode][1] = [path]
                    if debugPrint:
                        print("Updated")
                        print("Path is ", tentativeDistance[nextNode][1])
                        print('\t', tentativeDistance[nextNode])
    
            # Sort to find next node, the node with the smallest distance
            nextItem = []
            for key in tentativeDistance:
                nextItem.append([tentativeDistance[key][0], key])
            nextItem.sort()
            nextItem = np.array(nextItem)[:, 1]

            # check from shortest distance first whether node is unvisited
            for key in nextItem:
                if key in unvistedNodes:
                    break
            # Remove node from unvistedNodes 
            unvistedNodes.remove(key)
            node = key
            
            if debugPrint:
                print(unvistedNodes)
                print("Next node is ", node)
                for key in tentativeDistance:
                    print('\t', key, tentativeDistance[key])
                print('\n\n')
            
            if len(unvistedNodes) < 1:
                # There are no more unvisited nodes, the whole network has been
                # examined.
                carryOn = False
            
            if not self.endNode in unvistedNodes:
                # If wanting fastest result, as soon as endNode is visited the 
                # algorithm should stop
                carryOn = False

        # Check that a route is possible
        assert ~np.isinf(tentativeDistance[self.endNode][0]), 'There is no route between nodes'
                
        if debugPrint:
            print("Results from start node", self.startNode)
            print("Node: Dist > Route")
            for key in tentativeDistance:
                dist, links = tentativeDistance[key]
                lnkStr = ''
                for l in links: lnkStr += str(l) + ' '
                print('{}: {:4.0f} >'.format(key, dist), lnkStr)
            
        self.shortestPath = tentativeDistance[self.endNode][1]
        self.shortestPathDistance = tentativeDistance[self.endNode][0]
        
        # Displaying the information
        if True:
            # Print the results in the style requested.
            self.print_shortest_path()
        else:
            # Print shortest route along with distance information.
            print("\nShortest Path from {} to {} is:".format(self.startNode,
                                                             self.endNode))
            print(self.shortestPath)
            print("with length {}".format(self.shortestPathDistance))
            
        return self.shortestPath, self.shortestPathDistance


    def print_shortest_path(self):
        """ Displaying the shortest path in style required by task outline.
        
        This will display the route as one node per line. If multiple routes
        have been found, will display these while 
        """
        if len(self.shortestPath) == 1:
            for node in self.shortestPath[0]:
                print(node)
        else:
            # If multiple routes of the same length have been found, clearly 
            # display these different routes.
            for i in range(len(self.shortestPath)):
                print("Route Option {}/{}".format(i + 1, len(self.shortestPath)))
                for node in self.shortestPath[i]:
                    print(node)
                print()

if __name__ == '__main__':
    print("Scientific Programmer Task")
    print("by:        Chris Underwood")
    
    import time
    tic = time.perf_counter()
    setup_plotting()

    network = graph()
    if True:
        # Run script using this data, if not use parser 
        fp = "exmouth-links.dat"
        start_node = "J1006"
        # end_node = "J1002"
        end_node = "X1058" 
                
        #### Testing networks.
        # fp = "testing_data/test-2loops.dat"
        # fp = "testing_data/test_3-links.dat"
        # fp = "testing_data/test_4-links.dat"
        # fp = "testing_data/test_2-links.dat"
        # start_node = 'G'
        # end_node = 'H'
    
        network.fileName = fp
        network.startNode = start_node
        network.endNode = end_node
    else:
        network.parser()
    
    network.loadGraph()
    # Display the network
    G = network.displayInputtedNetwork(figsize=(8, 6))
    shortest_route, distance = network.calc_shortest_path_Dijkstra(
            debugPrint=False)
    
    toc = time.perf_counter()
    print("Script Run Time {:0.4f} seconds".format(toc - tic))
    