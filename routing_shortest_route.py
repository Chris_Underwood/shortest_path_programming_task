#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Routing Script

Created on Fri Jan 22 12:45:39 2021
@author: chrisunderwood

Script is run as:
    python routing_shortest_route.py -f <Name of data file> -s <Start Node> -e <End Node>
example:
    python routing_shortest_route.py -f "exmouth-links.dat" -s "J1002" -e "J1060"
"""
from Shortest_Path_Between_Nodes import graph

network = graph()
network.parser()

network.loadGraph()
network.calc_shortest_path_Dijkstra()

    
    
