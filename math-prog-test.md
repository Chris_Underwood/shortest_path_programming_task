
# Scientific Programmer Task

Graphs and graph algorithms are central to transport modelling. We would like
you to implement an algorithm to find the shortest path between any pair of
nodes in a graph.

A sample road network is provided here:

https://gist.github.com/byrney/a64f62850d1c5f5ca287ce14dec5f7ad

The format of the file is

```
startNode  endNode  distance
A          B        2
B          C        4
```

Nodes may be composed of letters and numbers (but not spaces). Distances
are always positive integers

Links are directional so if A-B is listed but B-A is not then you can travel
from A to B but not from B to A.


Write a commandline application that can be run to print shortest paths like
this:

```
command <network-filename> <origin> <destination>
```

and will print the list of nodes which are visited by the shortest path. For
example:

```
> python my-fancy-routing.py exmouth-links.dat J1053 J1037

J1053
J1035
J1036
J1037
```

If there is no path, print a message and exit.

Requirements:

* Use libraries as necessary but code the algorithm yourself
* Provide references to your chosen algorithm in the readme (a Wikipedia link will do)
* It should run for any pair of nodes in this network in less than 1 second
* It should be possible to run it with a different network (in the same
  format)
* Make the project and code available by making it public or sharing it [^1]
* Include a readme outlining the steps to 'build' the project, run the
  testcases and run the command (so we can try it)

[^1]: byrney on github, rpbyrne on bitbucket or byrney on gitlab.com. Both
      gitlab and bitbucket provide private repos for free.

