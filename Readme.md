# Scientific Programmer Task
#### Author: Chris Underwood
#### Email: christopher.underwood@york.ac.uk
___

A python script to find the shortest path between two nodes in a graph.


Script is run as:
```
    python routing_shortest_route.py -f <Name of data file> -s <Start Node> -e <End Node>
```
example:
```
    python routing_shortest_route.py -f "exmouth-links.dat" -s "J1002" -e "J1060"
```

where the data file is in the format

```
startNode  endNode  distance
A          B        2
B          C        4
```


## Building 

Build using pythons virtualenv which can be installed with: 
``` 
pip install virtualenv
```

I have run with virtualenv version 16.6.2


Then run the following commands:

```
virtualenv venv
virtualenv -p <Path to python 3.7>
source venv/bin/activate
pip install -r requirements.txt

```
The command line application can now be run.

## Script run time.
On my computer the script ```Shortest_Path_Between_Nodes.py``` takes around ~<0.025 seconds to run when no visualisation is taking place

Using Bash's time function:
```
time python routing_shortest_route.py -f "exmouth-links.dat" -s "J1002" -e "X1058"
```
I get run times of 

```
real	0m0.738s
user	0m0.618s
sys		0m0.110s
```


## Tests
The test can be run from the command line with either
``` python test_Shortest_Path_Between_Nodes.py ```
or
``` pytest ```

## References 
For this task the following pages were used

1. [Towards Data Science](https://towardsdatascience.com/10-graph-algorithms-visually-explained-e57faa1336f3)
2. [Dijkstra's algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm)

The alternate route finding algorithm, Bellman–Ford algorithm, was not implemented due to:

1. It is slower than the Dijkstra algorithm.
2. It can work with negative edges values, but this was not required for the task.
